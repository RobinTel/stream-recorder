const { Retriever: { Port: PORT }, MongoConfig: { Database, MongoURI } } = JSON.parse(require('fs').readFileSync(require('path').join(__dirname, '..', 'config.json')).toString())

//
const URL = require('url');
const HTTP = require('http');
const MongoDB = require('mongodb');
const RemoteAuthorization = require('../remoteAuthorization');
const MongoBinary = MongoDB.Binary;
const Server = HTTP.createServer({ insecureHTTPParser: true });
Server.on('error', console.error);
Server.on('listening', console.log.bind(console, 'retriever port: ' + PORT));
const MongoClient = new MongoDB.MongoClient(MongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: require('os').cpus().length
});
let chunksCollection;
let filesCollection;
MongoClient.connect().then(() => {
    chunksCollection = MongoClient.db(Database).collection('fs.chunks');
    filesCollection = MongoClient.db(Database).collection('fs.files');
    Server.listen({ port: PORT, exclusive: false });
}).catch(console.error);
Server.headersTimeout = 5 * 1000;
Server.keepAliveTimeout = 5 * 1000;
Server.timeout = 5 * 1000;
Server.maxHeadersCount = 25;
Server.on('request', async (request, response) => {
    let cursor;
    try {
        request.on('upgrade', () => {
            request.destroy();
        });
        request.on('timeout', () => {
            request.destroy();
        });
        request.on('data', () => {
            request.destroy();
        });
        if ((request.method != 'GET' && request.method != 'HEAD') ||
            typeof request.url !== 'string' ||
            request.url.length < 12 ||
            typeof request.headers.authorization !== 'string')
            request.destroy();

        let { query } = URL.parse(request.url, true);
        if (typeof query.id !== 'string' || query.id.length < 12) {
            request.destroy();
        }
        let _id = new MongoDB.ObjectID(query.id);
        await RemoteAuthorization(request.headers.authorization);
        request.connection.setNoDelay(true);
        let file = await filesCollection.findOne({
            _id
        }, { projection: { done: 1, contentType: 1, createdAt: 1 } });
        let noBody = request.method === 'HEAD';
        if (file) {
            let lastChunk = await chunksCollection.findOne({ files_id: _id }, { sort: { _id: -1 }, projection: { _id: 1 } });
            let eTag;
            response.statusCode = 200;
            if (lastChunk) {
                eTag = '"' + lastChunk._id.toString() + '"';
                if (eTag === request.headers['if-none-match'])
                    response.statusCode = 304;
            } else {
                response.statusCode = 204;
                noBody = true;
            }
            response.setHeader('Transfer-Encoding', 'chunked')
            response.setHeader('Content-Type', file.contentType || 'application/octet-stream');
            response.setHeader('E-Tag', eTag);
            response.setHeader('Created-At', new Date(file.createdAt * 1000).toGMTString());
            if (file.done === 1)
                response.setHeader('Done', '1');
        } else {
            response.statusCode = 404;
            throw new Error('not found');
        }
        if (noBody) return response.end();
        //if (!request.headers.offset) request.headers.offset = 0;
        cursor = chunksCollection.find({ files_id: _id /*, _id: { $gte: request.headers.offset } */ }, { projection: { data: 1 }, sort: { _id: 1 } });
        let had = false;
        while (await cursor.hasNext()) {
            had = true;
            if (request.aborted) {
                throw new Error();
            }
            response.write((await cursor.next()).data.value());
        }
        if (!had) {
            response.statusCode = 204;
        }
        response.end();
    } catch (e) {
        try {
            if (!response.statusCode || response.statusCode === 200) {
                response.statusCode = 400;
            }
        } catch (_) { }
        try {
            response.end(e.message);
        } catch (_) { }
        try {
            request.destroy();
        } catch (_) { }
    } finally {
        try {
            cursor.close();
        } catch (_) { }
    }
});
