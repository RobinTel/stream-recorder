const { AuthURL } = JSON.parse(require('fs').readFileSync(require('path').join(__dirname, '..', 'config.json')).toString())
const HTTP = require('http');
module.exports = async (authorization) => {
    return new Promise((res, rej) => {
        HTTP.get(AuthURL, { headers: { authorization, timeout: 5 * 1000 } }, (response) => {
            if (response.statusCode === 200) res();
            else rej(new Error('unauthorized'));
        }).on('error', rej);
    });
};