const { Recorder: { Port: PORT }, MongoConfig: { Database, MongoURI } } = JSON.parse(require('fs').readFileSync(require('path').join(__dirname, '..', 'config.json')).toString())
//
const URL = require('url');
const HTTP = require('http');
const MongoDB = require('mongodb');
const MongoBinary = MongoDB.Binary;
const RemoteAuthorization = require('../remoteAuthorization');
const Server = HTTP.createServer({ insecureHTTPParser: true });
Server.on('error', console.error);
Server.on('listening', console.log.bind(console, 'recorder port: ' + PORT));
const MongoClient = new MongoDB.MongoClient(MongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: require('os').cpus().length
});
let chunksCollection;
let filesCollection;
MongoClient.connect().then(() => {
    chunksCollection = MongoClient.db(Database).collection('fs.chunks');
    filesCollection = MongoClient.db(Database).collection('fs.files');
    Server.listen({ port: PORT, exclusive: false });
}).catch(console.error);
Server.headersTimeout = 5 * 1000;
Server.keepAliveTimeout = 5 * 1000;
Server.timeout = 5 * 1000;
Server.maxHeadersCount = 25;
Server.on('request', async (request, response) => {
    try {
        request.on('upgrade', () => {
            request.destroy();
        });
        request.on('timeout', () => {
            request.destroy();
        });
        if (request.method != 'POST' ||
            typeof request.url !== 'string' ||
            request.url.length < 12 ||
            typeof request.headers.authorization !== 'string')
            request.destroy();
        let { query } = URL.parse(request.url, true);
        if (typeof query.id !== 'string' || query.id.length < 12) {
            request.destroy();
        }
        let _id = new MongoDB.ObjectID(query.id);
        await RemoteAuthorization(request.headers.authorization);
        request.connection.setNoDelay(false);
        let file = await filesCollection.findOne({
            _id
        }, { projection: { done: 1, auth: 1 } });
        if (file) {
            if (file.done) {
                response.statusCode = 423;
                throw new Error('not writable');
            }
            if (file.auth !== request.headers.authorization)
                throw new Error('access denied');
        } else {
            await filesCollection.insertOne({
                _id, auth: request.headers.authorization,
                createdAt: Date.now() / 1000,
                contentType: request.headers['content-type']
            });
        }
        request.on('end', () => {
            try {
                if (request.headers.last === '1') {
                    try {
                        //check for empty files
                        chunksCollection.findOne({ files_id: _id }, { projection: { files_id: 1 } }).then((found) => {
                            if (found) filesCollection.updateOne({ _id }, { $set: { done: true } }).catch(ignore);
                        }).catch(ignore);
                    } finally { }
                }
                response.statusCode = 204;
                response.end();
            } catch (_) { }
        });
        request.on('data', async  chunk => {
            try {
                let binary = new MongoBinary(chunk);
                await chunksCollection.insertOne({
                    files_id: _id, createdAt: Date.now() / 1000,
                    length: binary.length(), data: binary
                });
            } catch (e) {
                try {
                    response.statusCode = 500;
                    response.end(e.message);
                } catch (_) { }
                request.destroy();
            }
        });
    } catch (e) {
        try {
            if (!response.statusCode || response.statusCode === 200) {
                response.statusCode = 400;
            }
        } catch (_) { }
        try {
            response.end(e.message);
        } catch (_) { }
        try {
            request.destroy();
        } catch (_) { }
    }
});
function ignore(e) { console.error(e) }