const { Remover: { Port: PORT }, MongoConfig: { Database, MongoURI } } = JSON.parse(require('fs').readFileSync(require('path').join(__dirname, '..', 'config.json')).toString())

//
const URL = require('url');
const HTTP = require('http');
const MongoDB = require('mongodb');
const RemoteAuthorization = require('../remoteAuthorization');
const MongoBinary = MongoDB.Binary;
const Server = HTTP.createServer({ insecureHTTPParser: true });
Server.on('error', console.error);
Server.on('listening', console.log.bind(console, 'recorder port: ' + PORT));
const MongoClient = new MongoDB.MongoClient(MongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    poolSize: require('os').cpus().length
});
let chunksCollection;
let filesCollection;
MongoClient.connect().then(() => {
    chunksCollection = MongoClient.db(Database).collection('fs.chunks');
    filesCollection = MongoClient.db(Database).collection('fs.files');
    Server.listen({ port: PORT, exclusive: false });
}).catch(console.error);
Server.headersTimeout = 5 * 1000;
Server.keepAliveTimeout = 5 * 1000;
Server.timeout = 5 * 1000;
Server.maxHeadersCount = 25;
Server.on('request', async (request, response) => {
    try {
        if (request.method != 'DELETE' ||
            typeof request.url !== 'string' ||
            request.url.length < 12 ||
            typeof request.headers.authorization !== 'string')
            request.destroy();
        let { query } = URL.parse(request.url, true);
        if (typeof query.id !== 'string' || query.id.length < 12) {
            request.destroy();
        }
        let _id = new MongoDB.ObjectID(query.id);
        await RemoteAuthorization(request.headers.authorization);
        await chunksCollection.deleteMany({ files_id: _id });
        await filesCollection.deleteMany({ _id });
        response.end();
    } catch (e) {
        try {
            if (!response.statusCode || response.statusCode === 200) {
                response.statusCode = 400;
            }
        } catch (_) { }
        try {
            response.end(e.message);
        } catch (_) { }
        try {
            request.destroy();
        } catch (_) { }
    }
});
function ignore() { }
